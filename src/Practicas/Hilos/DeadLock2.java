/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Practicas.Hilos;

import java.util.logging.Level;
import java.util.logging.Logger;


public class DeadLock2 {
    static int a = 0;
    static int b = 0;
    
    static Object llave1 = new Object();
    static Object llave2 = new Object();

    static class Tarea1 extends Thread{
        
        @Override
        public void run(){
            System.out.println("Iniciando Tarea 1");
            
            synchronized(llave1){
                a = 10;
                System.out.println("Tarea 1, modificado el valor de a");
                
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(DeadLock2.class.getName()).log(Level.SEVERE, null, ex);
                }
                synchronized(llave2){
                    b = 20;
                }
            }
            
            System.out.printf("Tarea 1 - Valores a = %d y b = %d\n", a, b);
        }
    }
    
    static class Tarea2 extends Thread{
        
        @Override
        public void run(){
            
            System.out.println("Iniciando Tarea 2");
            
            synchronized(llave1){
                b = 30;
                
                System.out.println("Tarea 2, modificado el valor de b");
                try {
                    Thread.sleep(8000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(DeadLock2.class.getName()).log(Level.SEVERE, null, ex);
                }                
                synchronized(llave2){
                    a = 40;
                }
            }         
            
            System.out.printf("Tarea 2 - Valores a = %d y b = %d\n", a, b);
        }
    
    }
    
    public static void main(String args[]){
        
        (new Tarea1()).start();
        (new Tarea2()).start();
    }
    
}
